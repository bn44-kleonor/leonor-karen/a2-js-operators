
let a = 2;
let b = 3;

//Check if variable a is even
console.log("Check if variable a=2 is even");
console.log((a%2) == 0);	//true - even

//Check if variable b is odd
console.log("Check if variable b=3 is odd");
console.log((b%2) != 0);	//false - odd
